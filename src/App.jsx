import {useState} from "react";
import { Route, Routes } from "react-router-dom"
import Header from "./components/Header/Header"
import Footer from "./components/Footer/Footer"
import HomePage from "./components/Pages/HomePage"
import { useLocalStorage } from "./helpers/useLocalStorage"


function App() {
	let [favorites, setFavorites] = useLocalStorage([], 'favorite');
	let [baskets, setBaskets] = useLocalStorage([], 'basket');
	let [currentItem, setCurrentItem] = useState([]);
	const [isModalImage, setIsModalImage] = useState(false);
	const [isModalConfirmed, setIsModalConfirmed] = useState(false);


	const handleFavorites = (item) => {

		const isAdded = favorites.some((favorite)=> favorite.id === item.id)
		console.log(item, isAdded);

		if(isAdded){
			setFavorites(favorites.filter(i => i.id !== item.id))
		}
		else {
			setFavorites([...favorites,item])
		}
		
	}

	const handleBasket = (item) => {

		setIsModalConfirmed(false);
		setCurrentItem([]);
		const isAddedBasket = baskets.some((basket)=> basket.id === item.id)
		setCurrentItem([...baskets,item])
	}
	
	console.log(isModalConfirmed);

	const handleConfirmed = () => {
		setIsModalConfirmed(!isModalConfirmed);
		setBaskets(currentItem);
	}

    const handleModalImage = () => setIsModalImage(!isModalImage);
    

	

	return (
		<div className="g-app">
			<Header favorite={favorites} basket={baskets} />
			<main className="g-content">
				<Routes>
					<Route path="/" element={<HomePage handleFavorites={handleFavorites} handleBasket={handleBasket} handleModalImage={handleModalImage} isModalImage={isModalImage} handleConfirmed={handleConfirmed} favorites={favorites} />}/>
					<Route path="/favorite" element={<div>favorite</div>}/>
				</Routes>
			</main>
			<Footer/>
		</div>
	)
}

export default App
