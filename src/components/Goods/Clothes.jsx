import {Swiper,SwiperSlide} from 'swiper/react';
import "swiper/css"
import GoodsItem from './GoodsItem';

const Clothes = ({item, date, handleFavorites, favorites, handleBasket, handleCurrentPost, handleModalImage}) =>{
    const goodsItem = date.map((item,index)=>(
        <SwiperSlide className="good__item">
            <GoodsItem item={item} handleFavorites={handleFavorites} favorites={favorites} handleBasket={handleBasket} handleCurrentPost={handleCurrentPost} key={index} handleModalImage={handleModalImage} />
        </SwiperSlide>
    ))

    return(
        <Swiper
            slidesPerView={5}
            spaceBetween={16}
            className="goods__wrapper"
            navigation={true}
            grabCursor={false}
            draggable={false}
            preventClicksPropagation={true}
            preventClicks={true}
            scrollbar={{draggable: false, hide: true}}
            slideToClickedSlide={false}
            pagination={{clickable: true}}
        >
          {goodsItem}
        </Swiper>
    )
}


export default Clothes
