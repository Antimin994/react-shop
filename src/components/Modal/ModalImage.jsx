import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"
import "./Modal.scss"

const ModalImage = ({handleOk, handleClose, isOpen}) =>{

    const handleOutside = (event) => {
        if(!event.target.closest(".modal")){
            handleClose()
        }
    }

    return(
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalClose click={handleClose}/>
                <ModalHeader>
                </ModalHeader>
                <ModalBody>
                    

                    <h4>Додати продукт до кошику</h4>
                    <p></p>
                </ModalBody>
                <ModalFooter textFirst="додати до кошику" clickFirst={() => {
                    handleOk()
                    handleClose()
                }}/>
            </Modal>
        </ModalWrapper>
    )
}
ModalImage.propTypes = {
    title: PropTypes.string,
    desk: PropTypes.string,
    handleOk: PropTypes.string,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalImage
