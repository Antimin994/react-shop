import {Link} from "react-router-dom";
import Navbar from "./Navbar";
import { AiFillStar } from "react-icons/ai";
import { AiOutlineShopping } from "react-icons/ai";
import cx from "classnames"
import "./Header.scss"

const Header = ({favorite, basket}) =>{

    return(
        <header className="g-header">
            <div className="container">
                <div className="header__wrapper">
                    <div className="header__logo">
                        <Link to="/" className="logo">
                        <img src="../../../src/components/Header/icons/logo.svg"></img>
                        </Link>
                    </div>
                    <Navbar/>
                    <div className="header__actions">
                        <div className="header__actions_item">
                        <AiFillStar className="favorite-item"></AiFillStar>
                            <span className={cx("icon-favorite", {'icon-favorite_hidden':(favorite.length < 1)})}>
                                <span className="count">{favorite.length}</span>
                            </span>
                            
                        </div>
                        <div className="header__actions_item">
                        <AiOutlineShopping className="favorite-item"></AiOutlineShopping>
                            <span className={cx("icon-favorite", {'icon-favorite_hidden':(basket.length < 1)})}>
                                <span className="count">{basket.length}</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}


export default Header
