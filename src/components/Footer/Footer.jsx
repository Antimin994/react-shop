import "./Footer.scss"

const Footer = () =>{
    return(
        <footer className="g-footer">
            <div className="copywriting">Developed by Anton Antimijchuk (c) 2023</div>
        </footer>
    )
}

export default Footer
