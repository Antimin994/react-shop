import Goods from "../Goods/Goods"
import ModalImage from "../Modal/ModalImage"
import PropTypes from 'prop-types'



const HomePage = (props) => {
 const {handleFavorites, handleBasket, isModalImage, favorites, handleModalImage, handleConfirmed} = props

    return (
        <>
          <Goods handleFavorites={handleFavorites} handleBasket={handleBasket}  handleModalImage={handleModalImage} favorites={favorites} />
          <ModalImage  isOpen={isModalImage} handleClose={handleModalImage} handleOk={handleConfirmed} />
        </>
    )
}

HomePage.propTypes = {
  handleFavorites: PropTypes.bool,
  handleBasket: PropTypes.bool,
  isModalImage: PropTypes.bool,
  handleModalImage: PropTypes.bool,
  handleConfirmed: PropTypes.bool,
  favorites: PropTypes.any
}

export default HomePage